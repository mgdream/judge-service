FROM ubuntu:latest

RUN apt-get -y update  && \
    apt-get -y upgrade

RUN DEBIAN_FRONTEND=noninteractive \
    apt-get -y install --no-install-recommends \
        openjdk-11-jdk

COPY target/judge-service.jar /app/

COPY target/libexecutor.so /usr/lib/

ENTRYPOINT ["java", "-jar", "/app/judge-service.jar"]