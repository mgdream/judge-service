#!/bin/bash

gcc -c src/test/native/test_time.c \
  -o target/objs/test_time.o

gcc target/objs/test_time.o \
  -o target/bin/test_time
