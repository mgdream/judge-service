#!/bin/bash

gcc -c src/test/native/test_exit_1.c \
  -o target/objs/test_exit_1.o

gcc target/objs/test_exit_1.o \
  -o target/bin/test_exit_1
