#!/bin/bash

gcc -c src/main/native/linux/cn_mgdream_judge_natives_Executor.c \
  -o target/objs/cn_mgdream_judge_natives_Executor.o \
  -I src/main/native/ \
  -I src/main/native/include \
  -I src/main/native/include/linux \
  -fPIC \
  -O3

gcc target/objs/cn_mgdream_judge_natives_Executor.o \
  -o target/libexecutor.so \
  -shared \
  -O3
