package cn.mgdream.judge.natives;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.junit.jupiter.api.Assertions.*;

class ExecutorTest {

    private static final Logger logger = LoggerFactory.getLogger(ExecutorTest.class);

    @Test
    void loadLibrary() {
        Executor executor = new Executor();
    }

    @Test
    void responseNotNull() {
        Executor executor = new Executor();
        Executor.Request request = new Executor.Request();
        Executor.Response response = executor.execute(request);
        assertNotNull(response, "Execute response must not be null");
    }

    @Test
    void responseStatusIsOne() {
        Executor executor = new Executor();
        Executor.Request request = new Executor.Request();
        request.setCommand("test_exit_1");
        Executor.Response response = executor.execute(request);
        assertEquals(response.getStatus(), 1);
    }

    @Test
    void responseTimeUsageGreaterThanZero() {
        Executor executor = new Executor();
        Executor.Request request = new Executor.Request();
        request.setCommand("test_time");
        Executor.Response response = executor.execute(request);
        assertTrue(response.getUserTime() + response.getSystemTime() > 0L);
    }
}