package cn.mgdream.judge.natives;

public class Executor {

    static {
        System.loadLibrary("executor");
    }

    public native Response execute(Request request);

    public static class Request {

        private String command;

        public String getCommand() {
            return command;
        }

        public void setCommand(String command) {
            this.command = command;
        }
    }

    public static class Response {

        private int status;

        private long systemTime;

        private long userTime;

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public long getSystemTime() {
            return systemTime;
        }

        public void setSystemTime(long systemTime) {
            this.systemTime = systemTime;
        }

        public long getUserTime() {
            return userTime;
        }

        public void setUserTime(long userTime) {
            this.userTime = userTime;
        }
    }
}
