#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include <unistd.h>

#include <sys/resource.h>
#include <sys/time.h>
#include <sys/wait.h>

#include <cn_mgdream_judge_natives_Executor.h>

static int64_t getResourceUsageSystemTime(const struct rusage *);
static int64_t getResourceUsageUserTime(const struct rusage *);
static const char **getStringSplitToken(const char *, char);
static const char *getRequestCommand(JNIEnv *, jobject);
static void releaseRequestCommand(JNIEnv *, jobject, const char *);
static jclass findResponseClass(JNIEnv *);
static jobject newResponse(JNIEnv *, jclass);
static void setResponseStatus(JNIEnv *, jobject, jint);
static void setResponseSystemTime(JNIEnv *, jobject, jlong);
static void setResponseUserTime(JNIEnv *, jobject, jlong);

/*
 * Class:     cn_mgdream_judge_natives_Executor
 * Method:    execute
 * Signature: (Lcn/mgdream/judge/natives/Executor/Request;)Lcn/mgdream/judge/natives/Executor/Response;
 */
JNIEXPORT jobject JNICALL Java_cn_mgdream_judge_natives_Executor_execute(JNIEnv *env, jobject self, jobject request)
{
    const char *command = getRequestCommand(env, request);
    jobject response = newResponse(env, findResponseClass(env));

    // If command is NULL, there is no need to execute anything,
    // all fields should be default values ( zero or null )
    if (command == NULL)
        return response;

    // Execute command in sub-process, and parent process as an observer
    // to observe the status of sub-process
    pid_t pid = fork();

    if (pid == 0)
    {
        const char **args = getStringSplitToken(command, ' ');
        _exit(execvp(args[0], args));
    }

    int wstatus;
    waitpid(pid, &wstatus, NULL);

    if (WIFEXITED(wstatus))
        setResponseStatus(env, response, WEXITSTATUS(wstatus));

    struct rusage rusage;
    getrusage(RUSAGE_CHILDREN, &rusage);

    setResponseSystemTime(env, response, getResourceUsageSystemTime(&rusage));
    setResponseUserTime(env, response, getResourceUsageUserTime(&rusage));

    releaseRequestCommand(env, request, command);
    return response;
}

static int64_t getResourceUsageSystemTime(const struct rusage *rusage)
{
    struct timeval stime = rusage->ru_stime;
    return stime.tv_sec * 1000 + stime.tv_usec / 1000;
}

static int64_t getResourceUsageUserTime(const struct rusage *rusage)
{
    struct timeval utime = rusage->ru_utime;
    return utime.tv_sec * 1000 + utime.tv_usec / 1000;
}

static const char **getStringSplitToken(const char *string, char delimiter)
{
    const char *stringIterator;
    const char *lastStringIterator;
    char *stringToken;
    char **stringArray;

    int stringTokenLength = 0;
    int stringArrayCapacity = 0;
    int stringArrayLength = 0;

    stringIterator = string;
    while (*stringIterator)
    {
        // Find the first not delimiter character character,
        // all character before this is delimiter character
        while (*stringIterator && *stringIterator == delimiter)
            stringIterator++;

        // Find the first delimiter character,
        // all character before this is not delimiter character
        lastStringIterator = stringIterator;
        while (*stringIterator && *stringIterator != delimiter)
            stringIterator++;

        // There is no delimiter character or not delimiter character,
        // so string iterator is reached the end of character string
        if (stringIterator == lastStringIterator)
            stringToken = NULL;
        else
        {
            stringTokenLength = stringIterator - lastStringIterator;

            // Allocate memory spaces for null-terminated character string
            stringToken = malloc(sizeof(char) * (stringTokenLength + 1));

            // Copy characters from last not delimiter character to
            // the first delimiter character
            strncpy(stringToken, lastStringIterator, stringTokenLength);

            // Null-terminated
            stringToken[stringTokenLength] = '\0';
        }

        // Enlarge string array if necessary,
        // at last 1 capacity for NULL
        if (stringArrayCapacity == 0)
            stringArray = malloc(stringArrayCapacity = 2);
        else if (stringArrayLength + 1 == stringArrayCapacity)
            stringArray = realloc(stringArray, (stringArrayCapacity <<= 1));

        // Store string token to the last of string array
        stringArray[stringArrayLength++] = stringToken;
    }

    // Null-terminated pointer array
    if (stringArray[stringArrayLength - 1])
        stringArray[stringArrayLength] = NULL;

    // Trim unused memory spaces
    stringArray = realloc(stringArray, stringArrayLength);

    return stringArray;
}

static const char *getRequestCommand(JNIEnv *env, jobject request)
{
    jclass requestClass = (*env)->GetObjectClass(env, request);
    jfieldID requestCommandFieldId = (*env)->GetFieldID(env, requestClass, "command", "Ljava/lang/String;");
    jstring requestCommandField = (*env)->GetObjectField(env, request, requestCommandFieldId);
    if (requestCommandField != NULL)
        return (*env)->GetStringUTFChars(env, requestCommandField, JNI_FALSE);
    return NULL;
}

static void releaseRequestCommand(JNIEnv *env, jobject request, const char *command)
{
    if (command != NULL)
    {
        jclass requestClass = (*env)->GetObjectClass(env, request);
        jfieldID requestCommandFieldId = (*env)->GetFieldID(env, requestClass, "command", "Ljava/lang/String;");
        jstring requestCommandField = (*env)->GetObjectField(env, request, requestCommandFieldId);
        (*env)->ReleaseStringUTFChars(env, requestCommandField, command);
    }
}

static jclass findResponseClass(JNIEnv *env)
{
    jclass responseClass = (*env)->FindClass(env, "cn/mgdream/judge/natives/Executor$Response");
    return responseClass;
}

static jobject newResponse(JNIEnv *env, jclass clazz)
{
    jmethodID initResponse = (*env)->GetMethodID(env, clazz, "<init>", "()V");
    jobject response = (*env)->NewObject(env, clazz, initResponse);
    return response;
}

static void setResponseStatus(JNIEnv *env, jobject response, jint status)
{
    jclass responseClass = (*env)->GetObjectClass(env, response);
    jfieldID responseStatusFieldId = (*env)->GetFieldID(env, responseClass, "status", "I");
    (*env)->SetIntField(env, response, responseStatusFieldId, status);
}

static void setResponseUserTime(JNIEnv *env, jobject response, jlong userTime)
{
    jclass responseClass = (*env)->GetObjectClass(env, response);
    jfieldID responseUserTimeFieldId = (*env)->GetFieldID(env, responseClass, "userTime", "J");
    (*env)->SetIntField(env, response, responseUserTimeFieldId, userTime);
}

static void setResponseSystemTime(JNIEnv *env, jobject response, jlong systemTime)
{
    jclass responseClass = (*env)->GetObjectClass(env, response);
    jfieldID responseUserTimeFieldId = (*env)->GetFieldID(env, responseClass, "systemTime", "J");
    (*env)->SetIntField(env, response, responseUserTimeFieldId, systemTime);
}
